package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class RecyclableDemon extends Genie {

    private String name = "RecyclableGenie";
    private boolean used = false;

    public RecyclableDemon(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public void grantWish() {
        if (!used) {
            super.grantWish();
            System.out.println("Granted!");
        } else {
            System.out.println(name + ": No more wishes!");
        }
    }

    public boolean getUsed(){
        return used;
    }

    public void setTrue(){
        used = true;
    }
}
