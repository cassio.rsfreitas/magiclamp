package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class FriendlyGenie extends Genie {

    private String name = "FriendlyGenie";

    public FriendlyGenie(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public void grantWish() {
        if (super.getGrantedWishes() < super.getMaxWishes()) {
            super.grantWish();
            System.out.println("Granted!");
        } else {
            System.out.println(name + ": No more wishes!");
        }
    }
}
