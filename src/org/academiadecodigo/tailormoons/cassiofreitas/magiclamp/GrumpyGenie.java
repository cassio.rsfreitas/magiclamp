package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class GrumpyGenie extends Genie {

    private String name = "GrumpyGenie";

    public GrumpyGenie (int maxWishes){
        super(maxWishes);
    }

    @Override
    public void grantWish() {
        if (super.getGrantedWishes() < 1) {
            super.grantWish();
            System.out.println("Granted!");
        } else {
            System.out.println(name + ": No more wishes!");
        }
    }
}
