package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class Genie {

    private int maxWishes;
    private int grantedWish = 0;

    public Genie(int maxWishes) {
        this.maxWishes = maxWishes;
    }

    public void grantWish() {
        grantedWish++;
    }

    public int getGrantedWishes() {
        return grantedWish;
    }

    public int getMaxWishes() {
        return maxWishes;
    }

}
