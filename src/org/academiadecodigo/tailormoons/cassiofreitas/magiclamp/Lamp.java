package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class Lamp {

    private int maxGenies;
    private int typeGenies = 1;
    private int generatedGenies = 0;
    private int resets = 0;

    public Lamp(int maxGenies) {
        this.maxGenies = maxGenies;
    }

    public Genie rubbed() {
        Genie newGenie = nextGenie();
        typeGenies++;
        return newGenie;
    }

    private Genie nextGenie() {
        Genie nextGenie;
        if (typeGenies <= maxGenies) {
            if (typeGenies % 2 == 0) {
                nextGenie = new GrumpyGenie(3);
                generatedGenies++;
            } else {
                nextGenie = new FriendlyGenie(3);
                generatedGenies++;
            }
        } else {
            nextGenie = new RecyclableDemon(3);
            generatedGenies++;
        }
        return nextGenie;
    }

    public void reset(RecyclableDemon genie) {
        boolean state = genie.getUsed();
        if (state) {
            System.out.println("This genie cannot be used!");
        } else {
            typeGenies = 1;
            genie.setTrue();
            System.out.println("Lamp reset!");
        }
    }

    public boolean compare(Lamp lamp) {
        if ((this.maxGenies == lamp.maxGenies) &&
                (this.generatedGenies == lamp.generatedGenies) &&
                (this.resets == lamp.resets)) {
            return true;
        }
        return false;
    }

}
