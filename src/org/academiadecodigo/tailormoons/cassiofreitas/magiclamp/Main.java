package org.academiadecodigo.tailormoons.cassiofreitas.magiclamp;

public class Main {
    public static void main(String[] args) {

        Lamp lamp1 = new Lamp(4);
        Lamp lamp2 = new Lamp(4);

        System.out.println(lamp1.compare(lamp2));

        lamp1.rubbed();
        lamp1.rubbed();
        Genie genie = lamp1.rubbed();

        if (genie instanceof RecyclableDemon) {
            lamp1.reset((RecyclableDemon) genie);
        }

        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
    }
}
